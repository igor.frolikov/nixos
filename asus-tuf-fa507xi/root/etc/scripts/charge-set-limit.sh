#!/bin/sh
## Check if "charge-limit-value" is in valid range
## then set the "charge_control_end_threshold"
## To find your "BAT?" number use the following command:
##      upower -e | grep 'BAT'

FILE="/etc/scripts/charge-limit-value"
if test -f $FILE; then
  VALUE=$(cat $FILE)
  if [ $VALUE -ge 60 ] && [ $VALUE -le 100 ]; then
    echo $VALUE > /sys/class/power_supply/BAT1/charge_control_end_threshold
  fi
fi
