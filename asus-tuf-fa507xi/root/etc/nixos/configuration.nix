# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
#
# Useful link: https://github.com/NixOS/nixos-hardware/tree/master

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Bootloader
  boot = {
  
    loader = {
      timeout = 2;
      efi.canTouchEfiVariables = true;
      systemd-boot = {
        enable = true;
        editor = false;  # For security reasons
        configurationLimit = 10;
      };
    };
    
    consoleLogLevel = 0;
    initrd.verbose = false;
    plymouth.enable = true;
#     kernelModules = [ "kvm-amd" "k10temp" ];  # Moved to hardware-configuration.nix
    blacklistedKernelModules = [ "ath3k" ];  # ath3k is the Linux Bluetooth driver for Atheros AR3011/AR3012 Bluetooth chipsets.
    initrd.kernelModules = [ "amdgpu" ];
    # kernelPackages = pkgs.linuxPackages_latest;
    # kernelPackages = pkgs.linuxPackages_xanmod_latest;
    # kernelPackages = pkgs.linuxPackages_xanmod;
    kernelPackages = pkgs.linuxPackages_zen;

    kernelParams = [
      "quiet"
      "splash"
      "vga=current"
      "rd.systemd.show_status=false"
      "rd.udev.log_level=3"
      "udev.log_priority=3"
      "amd_pstate=active" 
    ];

  };

  # swap to RAM
  zramSwap = {
    enable = true;
    memoryPercent = 50;
    algorithm = "lzo-rle";  # lzo lzo-rle lz4 lz4hc 842 zstd
  };

    # Disable hibernation
  systemd.targets = {
  	sleep.enable = true;
  	suspend.enable = true;
  	hibernate.enable = false;
  	hybrid-sleep.enable = false;
  };
  services.logind.extraConfig = "HandleHibernateKey=ignore";

  # CPU speed & power optimizer for Linux
  services.power-profiles-daemon.enable = true;

#   # auto-cpufreq conflicts with power-profiles-daemon
#   # Use it for DE other than GNOME or KDE Plasma
#   services.auto-cpufreq.enable = true;
# 
#   # https://github.com/AdnanHodzic/auto-cpufreq/blob/v2.4.0/auto-cpufreq.conf-example
#   services.auto-cpufreq.settings = {
# 
#     charger = {
#       governor = "performance";
#       energy_performance_preference = "balance_performance";
#       platform_profile = "balanced";
#       turbo = "auto";
#     };
# 
#     battery = {
#       governor = "powersave";
#       energy_performance_preference = "power";
#       platform_profile = "quiet";
#       turbo = "never";
#     };
# 
#   };

  networking.hostName = "nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Moscow";


  # Select internationalisation properties.
  console = {
    # earlySetup = true;
    packages = [ pkgs.terminus_font ];
    font = "ter-u24b";
    keyMap = "ru";
    # useXkbConfig = true;
  };

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "ru_RU.UTF-8";
    LC_IDENTIFICATION = "ru_RU.UTF-8";
    LC_MEASUREMENT = "ru_RU.UTF-8";
    LC_MONETARY = "ru_RU.UTF-8";
    LC_NAME = "ru_RU.UTF-8";
    LC_NUMERIC = "ru_RU.UTF-8";
    LC_PAPER = "ru_RU.UTF-8";
    LC_TELEPHONE = "ru_RU.UTF-8";
    LC_TIME = "ru_RU.UTF-8";
  };

  # Enable the X11 windowing system.
  # You can disable this if you're only using the Wayland session.
  services.xserver.enable = true;

  # Enable the KDE Plasma Desktop Environment.
  services.displayManager.sddm = {
  	enable = true;
  	# wayland.enable = true;
  	autoNumlock = true;
  };
  services.desktopManager.plasma6.enable = true;
  environment.plasma6.excludePackages = with pkgs.kdePackages; [
    elisa
  ];

  # Configure keymap in X11
  services.xserver.xkb = {
    layout = "us,ru";
    variant = "";
  };

  # Enable touchpad support (enabled default in most desktopManager).
  services.libinput.enable = true;

  # Configure video drivers in X11
  services.xserver = {
    videoDrivers = [ 
      "modesetting"
      # "amdgpu"
      "nvidia"
    ];
  };

  # GPU setup
  hardware = {
    amdgpu.initrd.enable = true;
    graphics = {
      enable = true;
      enable32Bit = true;
      extraPackages = with pkgs; [
        amdvlk
        rocmPackages.clr.icd   # AMD proprietary
      ];
      extraPackages32 = with pkgs; [ driversi686Linux.amdvlk ];
    };
  };

  # # HIP (https://wiki.nixos.org/wiki/AMD_GPU)
  # # Most software has the HIP libraries hard-coded. You can work around it on NixOS by using:
  systemd.tmpfiles.rules = [
      "L+    /opt/rocm/hip   -    -    -     -    ${pkgs.rocmPackages.clr}"
    ];

  # NVIDIA discrete card setup
  hardware.nvidia = {
    # modesetting.enable = true;            # "false" by default
    powerManagement.enable = false;       # "true" by default
    powerManagement.finegrained = true;   # "false" by default

    # Use the NVidia open source kernel module (not to be confused with the
    # independent third-party "nouveau" open source driver).
    open = true;

    # Enable the Nvidia settings menu, accessible via `nvidia-settings`.
    nvidiaSettings = true;

    # Optionally, you may need to select the appropriate driver version for your specific GPU.
    # package = config.boot.kernelPackages.nvidiaPackages.stable;
  };

  hardware.nvidia.prime = {
    # sync.enable = true;
    offload = {
      enable = true;
      enableOffloadCmd = true; # Provides `nvidia-offload` command
    };
    # Make sure to use the correct Bus ID values for your system!
    nvidiaBusId = "PCI:1:0:0";
    amdgpuBusId = "PCI:102:0:0";
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # IPP everywhere capable printer
  services.avahi = {
    enable = true;
    nssmdns4 = true;
    openFirewall = true;
  };

  # Enable scanner support
  hardware.sane = {
      enable = true;
      # Brother scanners
      # brscan4.enable = true;
  };

  # Enable sound with pipewire.
  # hardware.pulseaudio.enable = false;
  # services.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # MPD setup (Used by Cantata player)
  services.mpd = {
    enable = true;
    musicDirectory = "/home/igorf/Music";
    user = "igorf";
    group = "audio";
    extraConfig = ''
      audio_output {
        type "pipewire"
        name "PipeWire Output"
      }
    '';

    # Optional:
    # network.listenAddress = "any"; # if you want to allow non-localhost connections
    startWhenNeeded = true; # systemd feature: only start MPD service upon connection to its socket
  };

  systemd.services.mpd.environment = {
    XDG_RUNTIME_DIR = "/run/user/1000";
  };

  # Bluetooth
  services.blueman.enable = true; # The only to get connected, which I found
  hardware.bluetooth = {
    enable = true; # enables support for Bluetooth
    powerOnBoot = true; # powers up the default Bluetooth controller on boot
    settings.General.Enable = "Source,Sink,Media,Socket";  # Enabling A2DP Sink
  };

  environment.etc = {
  	"wireplumber/bluetooth.lua.d/51-bluez-config.lua".text = ''
  		bluez_monitor.properties = {
  			["bluez5.enable-sbc-xq"] = true,
  			["bluez5.enable-msbc"] = true,
  			["bluez5.enable-hw-volume"] = true,
  			["bluez5.headset-roles"] = "[ hsp_hs hsp_ag hfp_hf hfp_ag ]"
  		}
  	'';
  			# ["bluez5.enable-aptx"] = true,
  			# ["bluez5.enable-aptx-hd"] = true,
  			# ["bluez5.enable-aptx-ll"] = true,
  			# ["bluez5.enable-aac"] = true,
  };

  # Using Bluetooth headset buttons to control media player
  systemd.user.services.mpris-proxy = {
      description = "Mpris proxy";
      after = [ "network.target" "sound.target" ];
      wantedBy = [ "default.target" ];
      serviceConfig.ExecStart = "${pkgs.bluez}/bin/mpris-proxy";
  };

  # Setup shell scripts and aliases:
  environment = {
    variables = {
      PATH = "$PATH:/etc/scripts";
      EDITOR = "micro";
#       VISUAL = "micro";
      XDG_DATA_HOME="$HOME/.local/share";
      # AMD_VULKAN_ICD = "RADV";
      # MANGOHUD = "1";
    };
    shellAliases = {
      ff = "fastfetch";
      rebuild = "sudo nixos-rebuild switch";
      upgrade = "sudo nixos-rebuild switch --upgrade && reboot";
      rollback = "sudo nixos-rebuild switch --rollback && reboot";
      repair = "sudo nixos-rebuild switch --repair";
      list-generations = "sudo nix-env --list-generations --profile /nix/var/nix/profiles/system";
      switch-generation-current = "sudo /run/current-system/bin/switch-to-configuration boot";
      switch-generation-number = "sudo nix-env --profile /nix/var/nix/profiles/system --switch-generation";
      delete-generations-old = "sudo nix-collect-garbage -d && sudo nixos-rebuild boot && reboot";
      delete-generations-number = "sudo nix-env --profile /nix/var/nix/profiles/system --delete-generations";
      garbage = "sudo nix-collect-garbage && nix-collect-garbage";
      optimise = "sudo nix-store --optimise";
      config = "micro /etc/nixos/configuration.nix";
      ssd0 = "sudo smartctl -a /dev/nvme0";
      ssd1 = "sudo smartctl -a /dev/nvme1";
      ls = "eza --icons --group-directories-first";
      l = "eza --icons --group-directories-first";
      ll = "eza --long --header --git --icons --group-directories-first";
      la = "eza --long --header --git --icons --group-directories-first --all";
      lt = "eza --long --header --git --icons --group-directories-first --all --tree";
      df = "df -h";
      e = "micro";
      channel = "sudo nix-channel --list | grep nixos";
    };
  };

  # # Smart and user-friendly command line shell
   users.defaultUserShell = pkgs.fish;
   programs.fish = {
     enable = true;
     interactiveShellInit = "
       function fish_greeting
         echo
         echo -n (set_color brcyan)'  ' (set_color normal)
         echo -n (date +'%a, %d %b %Y   ')
         
         echo -n (set_color black)'  '
         echo -n (set_color red)'  '
         echo -n (set_color green)'  '
         echo -n (set_color yellow)'  '
         echo -n (set_color blue)'  '
         echo -n (set_color magenta)'  '
         echo -n (set_color cyan)'  '
         echo -n (set_color white)'  '
         echo (set_color normal)

         echo -n (set_color brcyan)'      ' (set_color normal)
         echo -n (date +'%H:%M         ')
         
         echo -n (set_color brblack)'  '
         echo -n (set_color brred)'  '
         echo -n (set_color brgreen)'  '
         echo -n (set_color bryellow)'  '
         echo -n (set_color brblue)'  '
         echo -n (set_color brmagenta)'  '
         echo -n (set_color brcyan)'  '
         echo -n (set_color brwhite)'  '
         echo (set_color normal)
       end
     ";
   };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.igorf = {
    isNormalUser = true;
    description = "Igor Frolikov";
    extraGroups = [
      "networkmanager"
      "wheel"
      "audio"
      "video"
      "libvirtd"
      "scanner"
      "lp"
      "kvm"
    ];
    packages = with pkgs; [
    #  kdePackages.kate
    #  thunderbird
    ];
  };

  # Enable virtualisation and virt-manager
  virtualisation.libvirtd.enable = true;
  programs.virt-manager.enable = true;
  services.qemuGuest.enable = true;
  services.spice-vdagentd.enable = true;  # enable copy and paste between host and guest

  # Enable the Flakes feature and the accompanying new nix command-line tool
  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  nixpkgs.config.allowUnfree = true;  # Allow unfree packages
  programs = {
  	firefox.enable = true;  # Install firefox
  	starship.enable = true;  # A minimal, blazing fast, and extremely customizable prompt for any shell
  	kdeconnect.enable = true;  # KDE Connect provides several features to integrate your phone and your computer
  };
  services = {
  	# clamav = {  # Antivirus engine
  	#   daemon.enable = true;  # Whether to enable ClamAV clamd daemon
  	#   updater.enable = true; # Whether to enable ClamAV freshclam updater
  	# };
  	hardware.openrgb.enable = true; # Open source RGB lighting control
  	resilio.enable = true;  # Automatically sync files via secure, distributed technology
  	syncthing.enable = true;  # Open Source Continuous File Synchronization
  };

  # List packages installed in system profile. To search, run: $ nix search wget
  environment.systemPackages = with pkgs; [
  #  vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    # fuzzel            # Wayland-native application launcher, similar to rofi’s drun mode
    # xwayland-satellite  # Xwayland outside your Wayland compositor
    wget              # Tool for retrieving files using HTTP, HTTPS, and FTP
    git               # Distributed version control system
    micro             # Modern and intuitive terminal-based text editor
    fastfetch         # Feature-rich and performance oriented, neofetch like system information tool
    microfetch        # Microscopic fetch script in Rust, for NixOS systems
    btop              # Monitor of resources
    smartmontools     # Tools for monitoring the health of hard drives
    dmidecode         # Tool that reads information about your system's hardware from the BIOS according to the SMBIOS/DMI standard
    # clamav            # Antivirus engine designed for detecting Trojans, viruses, malware and other malicious threats
    # clamtk            # Easy to use, lightweight front-end for ClamAV (Clam Antivirus).
    google-chrome     # A freeware web browser developed by Google
    # vivaldi           # A Browser for our Friends, powerful and personal
    (vivaldi.overrideAttrs
      (oldAttrs: {
        dontWrapQtApps = false;
        dontPatchELF = true;
        nativeBuildInputs = oldAttrs.nativeBuildInputs ++ [pkgs.kdePackages.wrapQtAppsHook];
      }))
    vivaldi-ffmpeg-codecs  # Additional support for proprietary codecs for Vivaldi
    brave             # Privacy-oriented browser for Desktop and Laptop computers
    alacritty         # Cross-platform, GPU-accelerated terminal emulator
    onlyoffice-desktopeditors    # Office suite: text, spreadsheet and presentation editors
    libreoffice-qt6-fresh        # Comprehensive, professional-quality productivity suite
    projectlibre      # Project-Management Software similar to MS-Project
    kdePackages.skanlite    # Lite image scanning application
    kdePackages.kate  # KDE Advanced text editor
    # thunderbird-latest  # Full-featured e-mail client
    # mailspring        #  Beautiful, fast and maintained fork of Nylas Mail by one of the original authors
    kdePackages.partitionmanager # Manage the disk devices, partitions and file systems on your computer
    mc                # File Manager and User Shell for the GNU Project
    rar               # Utility for RAR archives
    xclip             # Tool to access the X clipboard from a console application
    wl-clipboard      # Command-line copy/paste utilities for Wayland
    eza               # Replacement for 'ls' written in Rust
    ventoy            # open source tool to create bootable USB drive
    fwupd             # Firmware update manager
    terminal-colors   # Script displaying terminal colors in various formats
    duf               # Disk Usage/Free Utility
    parted            # Create, destroy, resize, check, and copy partitions
    system-config-printer  # It uses IPP to configure a CUPS server and provides dBUS interface
    nordic            # Gtk and KDE themes using the Nord color pallete
    # (papirus-nord.override { accent = "frostblue3"; })
    qbittorrent       # Featureful free software BitTorrent client
    filezilla         # Graphical FTP, FTPS and SFTP client
    zoom-us           # video conferencing application
    # whatsapp-for-linux  # Whatsapp desktop messaging app
    # zapzap            # WhatsApp desktop application for Linux
    whatsie           # Feature rich WhatsApp Client for Desktop Linux
    telegram-desktop  # Telegram Desktop messaging app
    skypeforlinux     # Linux client for Skype
    obsidian          # Powerful knowledge base that works on top of a local folder of plain text Markdown files
    bitwarden         # A secure and free password manager for all of your devices
    qalculate-qt      # The ultimate desktop calculator
    # kdePackages.kdenlive   # Video editor
    # kdePackages.kwave      # Sound editor
    rawtherapee       # RAW converter and digital photo processing software
    # gimp              # The GNU Image Manipulation Program
    gimp-with-plugins # The GNU Image Manipulation Program
    inkscape          # Vector graphics editor
    veracrypt         # Free Open-Source filesystem on-the-fly encryption
    # syncthing         # Open Source Continuous File Synchronization
    # syncthingtray     # Tray application and Dolphin/Plasma integration for Syncthing
    # resilio-sync      # Automatically sync files via secure, distributed technology
    # vlc               # Cross-platform media player and streaming server
    haruna            # Open source video player built with Qt/QML and libmpv
    cantata           # Graphical client for MPD
    sayonara          # Sayonara music player
    deadbeef          # Ultimate Music Player for GNU/Linux
    kid3              # A simple and powerful audio tag editor
    flacon            # Extracts audio tracks from an audio CD image to separate tracks
    # dsf2flac          # A DSD to FLAC transcoding tool # build issues dsf2flac-unstable-2021-07-31.drv
    ffmpeg            # A complete, cross-platform solution to record, convert and stream audio and video
    vulkan-tools      # Khronos official Vulkan Tools and Utilities
    clinfo            # Print all known information about all available OpenCL platforms and devices in the system
    gpu-viewer        # Front-end to glxinfo, vulkaninfo, clinfo and es2_info
    pciutils          # Collection of programs for inspecting and manipulating configuration of PCI devices
    goverlay          # An opensource project that aims to create a Graphical UI to help manage Linux overlays
    mangojuice        # Convenient alternative to GOverlay for setting up MangoHud
    mangohud          # A Vulkan and OpenGL overlay for monitoring FPS, temperatures, CPU/GPU load and more
    protonup-qt       # GUI install and manage for Proton-GE and Luxtorpeda for Steam and Wine-GE for Lutris
    lutris            # Open Source gaming platform for GNU/Linux
#     wine              # An Open Source implementation of the Windows API on top of X, OpenGL, and Unix
    zeroad            # A free, open-source game of ancient warfare
    wineWowPackages.stable  # support both 32- and 64-bit applications
#     wineWowPackages.staging # wine-staging (version with experimental features)
#     wineWowPackages.waylandFull  # native wayland support (unstable)
    winetricks        # winetricks (all versions)
    freefilesync      # Open Source File Synchronization & Backup Software
    # celeste           # GUI file synchronization client that can sync with any cloud provider
    zed-editor        # High-performance, multiplayer code editor from the creators of Atom and Tree-sitter
    # evolution         # Personal information management application that provides integrated mail, calendaring and address book functionality
    freeoffice        # Office suite with a word processor, spreadsheet and presentation program
    pdfsam-basic      # Multi-platform software designed to extract pages, split, merge, mix and rotate PDF files
    drawio            # Desktop application for creating diagrams
  ];

  # Enable Flatpak support
  services.flatpak.enable = true;
  systemd.services.flatpak-repo = {
    wantedBy = [ "multi-user.target" ];
    path = [ pkgs.flatpak ];
    script = ''
      flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    '';
  };

  # Fonts             # Add your personal fonts to: ~/.local/share/fonts
  fonts = {
    fontDir.enable = true;
    packages = with pkgs; [
      montserrat
      roboto
      (nerdfonts.override { fonts = [ "Hack" ]; })
      # nerd-fonts.monaspace
    ];
# For all fonts use structure:
# fonts.packages = [ ... ] ++ builtins.filter lib.attrsets.isDerivation (builtins.attrValues pkgs.nerd-fonts)
    fontconfig.enable = true;
  };

  # # Set laptop battery charge limit
  # # Taken from https://github.com/NixOS/nixos-hardware/blob/master/asus/battery.nix
  #
  # # For ASUS laptops it can be replaced by:
  # services = {
  #   asusd = {
  #     enable = lib.mkDefault true;
  #     enableUserService = lib.mkDefault true;
  #   };
  #  environment.systemPackages = with pkgs; [ asusctl ];

  systemd.services.battery-charge-threshold = {
    wantedBy = [ "local-fs.target" "suspend.target" ];
    after = [ "local-fs.target" "suspend.target" ];
    description = "Set the battery charge threshold";
    startLimitBurst = 5;
    startLimitIntervalSec = 1;
    serviceConfig = {
      Type = "oneshot";
      Restart = "on-failure";
      # The script checks the value before setting it. That is more safe.
      ExecStart = "/etc/scripts/charge-set-limit.sh";
    };
  };

  nix = {
  # Deduplication process on your Nix store
    settings.auto-optimise-store = true;
  # Automatic garbage collection
    gc.automatic = true;
    gc.dates = "weekly";
    gc.options = "--delete-older-than 30d";
  };

  # Periodically run fstrim service
  services.fstrim = {
    enable = true;
    interval = "weekly";
  };

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  system.copySystemConfiguration = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.11"; # Did you read the comment?

}
