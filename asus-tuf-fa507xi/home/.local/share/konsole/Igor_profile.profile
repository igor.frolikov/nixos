[Appearance]
AntiAliasFonts=true
BorderWhenActive=false
ColorScheme=Nordic - edited
EmojiFont=Noto Color Emoji,11,-1,5,400,0,0,0,0,0,0,0,0,0,0,1
Font=Monospace,12,-1,5,400,0,0,0,0,0,0,0,0,0,0,1
IgnoreWcWidth=false
LineSpacing=0
UseFontLineChararacters=false
WordMode=true
WordModeAscii=true
WordModeAttr=false
WordModeBrahmic=false

[Cursor Options]
CursorShape=1

[General]
Environment=TERM=xterm-256color,COLORTERM=truecolor
Name=Igor_profile
Parent=FALLBACK/
TerminalCenter=false
TerminalColumns=150
TerminalMargin=5
TerminalRows=45

[Scrolling]
HighlightScrolledLines=false
HistorySize=10000
ScrollBarPosition=2

[Terminal Features]
BidiRenderingEnabled=true
BlinkingCursorEnabled=true
VerticalLine=false
VerticalLineAtChar=80
