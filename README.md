  
# nixos

### My personal config files and some tips
Feel free to use and modify the code

#### Partition boot SSD drive for NIXOS

    parted /dev/nvme0n1 -- mklabel gpt
    parted /dev/nvme0n1 -- mkpart primary 512MB 100%
    parted /dev/nvme0n1 -- mkpart ESP fat32 1MB 512MB

I do not use swap partition, I use `zramSwap` or swapfile (see `configuration.nix`)

#### Format boot SSD drive for NIXOS 

Options for my SSD 128GB:

    mkfs.ext4 -L nixos -T news -m 2 /dev/nvme0n1p1
    mkfs.fat -F 32 -n boot /dev/nvme0n1p2

`-T news` - set inode_ratio = 4096 (default is 16384, as described in mke2fs.conf), it increases number of nodes for NIX store.

`-m 2` - set the number of reserved block space to 2% (default is 5%, which is too much for drives over 50GB)

To change it to 1% afterwards, use: 

    tune2fs -m 1 /dev/nvme0n1p1

#### Format data HDD drive

Options for my data HDD 1TB

    mkfs.ext4 -L data -m 1 /dev/sda

it is safe for data drives to use `-m 0`, or change the value later: `tune2fs -m 0 /dev/sda1`.
